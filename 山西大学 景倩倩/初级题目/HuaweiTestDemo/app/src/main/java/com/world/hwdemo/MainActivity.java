package com.world.hwdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;

/**
 * 认证服务
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button huaweiid_button;
    private TextView result_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //按钮
        huaweiid_button = findViewById(R.id.huaweiid_button);
        //UID显示的控件
        result_text = findViewById(R.id.result_text);

        huaweiid_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        AGConnectAuth.getInstance().signIn(MainActivity.this, AGConnectAuthCredential.HMS_Provider)
                .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
            @Override
            public void onSuccess(SignInResult signInResult) {
                String uid = signInResult.getUser().getUid();

                Log.d("华为登录信息success", uid);

                result_text.setText(uid);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.d("华为登录信息fail", e.getMessage());
            }
        });
    }

}