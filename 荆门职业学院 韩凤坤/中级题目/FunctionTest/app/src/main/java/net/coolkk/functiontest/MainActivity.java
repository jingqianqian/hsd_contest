package net.coolkk.functiontest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.huawei.agconnect.function.AGCFunctionException;
import com.huawei.agconnect.function.AGConnectFunction;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "functionTest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button resultBtn = findViewById(R.id.rst_btn);
        TextView resultText = findViewById(R.id.rst_text);
        EditText inputYear = findViewById(R.id.year_text);

        resultBtn.setOnClickListener(v -> {
            String inputText = inputYear.getText().toString();
            if (inputText.equals("") || !isInputLegit(inputText)) {
                resultText.setText("输入错误");
            } else {
                HashMap<String, String> map = new HashMap<>();
                map.put("year", inputText);
                AGConnectFunction function = AGConnectFunction.getInstance();
                function.wrap("test-$latest").call(map)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                String value = task.getResult().getValue();
                                try {
                                    JSONObject object = new JSONObject(value);
                                    String result = (String) object.get("result");
                                    resultText.setText(result);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.i(TAG, value);
                            } else {
                                Exception e = task.getException();
                                if (e instanceof AGCFunctionException) {
                                    AGCFunctionException functionException = (AGCFunctionException) e;
                                    int errCode = functionException.getCode();
                                    String message = functionException.getMessage();
                                    Log.e(TAG, "errorCode: " + errCode + ", message: " + message);
                                }
                            }
                        });
            }
        });

    }

    private boolean isInputLegit(String input) {
        for (int i = 0; i < input.length(); i++) {
            System.out.println(input.charAt(i));
            if (!Character.isDigit(input.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}