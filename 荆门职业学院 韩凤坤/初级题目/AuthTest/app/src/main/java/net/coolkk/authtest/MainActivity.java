package net.coolkk.authtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "huaweiid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button huaweiidButton = findViewById(R.id.huaweiid_button);
        huaweiidButton.setOnClickListener(view -> AGConnectAuth.getInstance().signIn(MainActivity.this, AGConnectAuthCredential.HMS_Provider)
                .addOnSuccessListener(signInResult -> {
                    String uid = signInResult.getUser().getUid();
                    Log.i(TAG, "Uid: " + uid);
                    TextView textView = findViewById(R.id.result_text);
                    textView.setText(uid);
                }).addOnFailureListener(e -> Log.e(TAG, "signin failed, error: " + e.getMessage())));
    }
}