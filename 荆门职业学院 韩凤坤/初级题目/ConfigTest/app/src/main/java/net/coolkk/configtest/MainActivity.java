package net.coolkk.configtest;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.huawei.agconnect.remoteconfig.AGConnectConfig;

public class MainActivity extends AppCompatActivity {
    private static final String GREETING_KEY = "GREETING_KEY";
    private static final String SET_BOLD_KEY = "SET_BOLD_KEY";
    private AGConnectConfig config;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.mTextView);
        Button button = findViewById(R.id.mButton);
        config = AGConnectConfig.getInstance();
        config.applyDefault(R.xml.remote_config);
        textView.setText(config.getValueAsString(GREETING_KEY));
        if (config.getValueAsBoolean(SET_BOLD_KEY)) {
            textView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
        button.setOnClickListener(view -> fetchAndApply());

    }

    private void fetchAndApply() {
        config.fetch(0).addOnSuccessListener(configValues -> {
            config.apply(configValues);
            textView.setText(config.getValueAsString(GREETING_KEY));
            if (config.getValueAsBoolean(SET_BOLD_KEY)) {
                textView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }
        }).addOnFailureListener(e -> textView.setText("fetch setting failed: " + e.getMessage()));
    }

}