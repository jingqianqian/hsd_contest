package net.coolkk.exampleb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.applinking.AppLinking;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneConfig;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.Task;

import net.coolkk.exampleb.data.ObjectTypeInfoHelper;
import net.coolkk.exampleb.data.User;
import net.coolkk.exampleb.util.DBUtil;
import net.coolkk.exampleb.util.Tool;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CloudDBZone mCloudDBZone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //匿名登录
        AGConnectUser auth = Tool.login();
        User userNow = new User();
        userNow.setId(auth.getUid());
        userNow.setName(auth.getUid());
        //数据库
        try {
            //初始化
            AGConnectCloudDB.initialize(this);
            //实例
            AGConnectInstance instance = AGConnectInstance.getInstance();
            AGConnectCloudDB mCloudDB = AGConnectCloudDB.getInstance(instance, AGConnectAuth.getInstance(instance));
            mCloudDB.createObjectType(ObjectTypeInfoHelper.getObjectTypeInfo());
            //配置
            CloudDBZoneConfig mConfig = new CloudDBZoneConfig("QuickStartDemo", CloudDBZoneConfig.CloudDBZoneSyncProperty.CLOUDDBZONE_CLOUD_CACHE, CloudDBZoneConfig.CloudDBZoneAccessProperty.CLOUDDBZONE_PUBLIC);
            mConfig.setPersistenceEnabled(true);
            Task<CloudDBZone> openDBZoneTask = mCloudDB.openCloudDBZone2(mConfig, true);
            openDBZoneTask.addOnSuccessListener(cloudDBZone -> {
                mCloudDBZone = cloudDBZone;
                //登录用户数据库
                CloudDBZoneQuery<User> query = CloudDBZoneQuery.where(User.class).equalTo("id", userNow.getId());
                Task<CloudDBZoneSnapshot<User>> queryTask = mCloudDBZone.executeQuery(
                        query,
                        CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
                queryTask.addOnSuccessListener(snapshot -> {
                    try {
                        //老用户
                        User userTemp = snapshot.getSnapshotObjects().get(0);
                        DBUtil.updateUser(mCloudDBZone, userTemp);
                    } catch (AGConnectCloudDBException e) {
                        //新用户
                        DBUtil.updateUser(mCloudDBZone, userNow);
                        e.printStackTrace();
                    }
                });
                //新用户注册
                Intent intent = getIntent();
                if (intent.getStringExtra("userNewId") != null) {
                    //新用户注册-新用户
                    User userNew = new User();
                    userNew.setId(intent.getStringExtra("userNewId"));
                    userNew.setName(intent.getStringExtra("userNewName"));
                    userNew.setScore(intent.getIntExtra("userNewScore", 0));
                    DBUtil.updateUser(mCloudDBZone, userNew);
                    //新用户注册-老用户
                    DBUtil.setUserScore(mCloudDBZone, intent.getStringExtra("userShareId"), intent.getIntExtra("userShareScore", 0));
                }
            });
        } catch (AGConnectCloudDBException e) {
            e.printStackTrace();
        }
        //分享
        Button buttonShare = findViewById(R.id.buttonReg);
        buttonShare.setOnClickListener(v -> {
            AppLinking.Builder builder = AppLinking.newBuilder()
                    .setUriPrefix("https://coolkktest.drcn.agconnect.link")
                    .setDeepLink(Uri.parse("https://coolkk.net/?name=" + userNow.getId() + "&id=" + userNow.getId() + "&price=200"))
                    .setAndroidLinkInfo(AppLinking.AndroidLinkInfo.newBuilder()
                            .setAndroidDeepLink("invite://coolkk.net/?name=" + userNow.getId() + "&id=" + userNow.getId() + "&price=200")
                            .build())
                    .setCampaignInfo(AppLinking.CampaignInfo.newBuilder()
                            .setName("1")
                            .setSource("ShareInApp")
                            .setMedium("ExampleApp")
                            .build())
                    .setPreviewType(AppLinking.LinkingPreviewType.AppInfo);

            Uri applinkgUri = builder.buildAppLinking().getUri();

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, applinkgUri.toString());
            shareIntent = Intent.createChooser(shareIntent, "选择分享");
            startActivity(shareIntent);
        });
        //查询
        Button buttonQuery = findViewById(R.id.buttonQuery);
        buttonQuery.setOnClickListener(v -> {
            CloudDBZoneQuery<User> query = CloudDBZoneQuery.where(User.class).equalTo("id", userNow.getId());
            Task<CloudDBZoneSnapshot<User>> queryTask = mCloudDBZone.executeQuery(
                    query,
                    CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
            queryTask.addOnSuccessListener(snapshot -> {
                try {
                    User userTemp = snapshot.getSnapshotObjects().get(0);
                    Tool.toast(this, "积分为：" + String.valueOf(userTemp.getScore()));
                } catch (AGConnectCloudDBException e) {
                    e.printStackTrace();
                }
            });
        });
    }
}