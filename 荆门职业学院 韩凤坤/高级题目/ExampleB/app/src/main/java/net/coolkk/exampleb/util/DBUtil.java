package net.coolkk.exampleb.util;

import android.util.Log;

import com.huawei.agconnect.cloud.database.CloudDBZone;
import com.huawei.agconnect.cloud.database.CloudDBZoneQuery;
import com.huawei.agconnect.cloud.database.CloudDBZoneSnapshot;
import com.huawei.agconnect.cloud.database.exceptions.AGConnectCloudDBException;
import com.huawei.hmf.tasks.Task;

import net.coolkk.exampleb.data.User;

public class DBUtil {

    public static void updateUser(CloudDBZone mCloudDBZone, User user) {
        Task<Integer> upsertTaskNew = mCloudDBZone.executeUpsert(user);
        upsertTaskNew.addOnSuccessListener(cloudDBZoneResult -> {
            Log.d("examplebLog", "userNew");
        }).addOnFailureListener(e -> Log.d("examplebLog", e.getMessage()));
    }

    public static void setUserScore(CloudDBZone mCloudDBZone, String id, int score) {
        CloudDBZoneQuery<User> query = CloudDBZoneQuery.where(User.class).equalTo("id", id);
        Task<CloudDBZoneSnapshot<User>> queryTask = mCloudDBZone.executeQuery(
                query,
                CloudDBZoneQuery.CloudDBZoneQueryPolicy.POLICY_QUERY_FROM_CLOUD_ONLY);
        queryTask.addOnSuccessListener(snapshot -> {
            try {
                User user = snapshot.getSnapshotObjects().get(0);
                user.setScore(user.getScore() + score);
                DBUtil.updateUser(mCloudDBZone, user);
            } catch (AGConnectCloudDBException e) {
                e.printStackTrace();
            }
        });
    }
}
