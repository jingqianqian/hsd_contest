package com.fbb.message_application;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;

import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "AppMessaging";
    private AGConnectAppMessaging appMessaging;
    private Button addView;
    private Button rmView;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HiAnalyticsTools.enableLog();
        HiAnalyticsInstance instance = HiAnalytics.getInstance(this);
        instance.setUserProfile("userKey","value");
    }
}